extern crate gcc;

fn main() {
    let mut c = gcc::Config::new();
    let target = std::env::var("TARGET").unwrap();
    let target =  match &target[..] {
        "i686-sel4-unknown" => "i686-unknown-linux-gnu",
        "arm-sel4-gnueabihf" => "arm-unknown-linux-gnueabihf",
        "arm-sel4-gnueabi" => "arm-unknown-linux-gnueabi",
        c => c,
    };
    c.target(target)
     .flag("-ffreestanding")
     .flag("-nodefaultlibs")
     .flag("-nostdlib")
     .file("malloc.c")
     .file("expand_heap.c")
     .file("calloc.c")
     .file("memalign.c")
     .file("malloc_usable_size.c");
    c.compile("libmusl_alloc.a");
}
